FROM node:8-alpine

COPY ./* /

RUN node install

CMD [ "npm test" ]