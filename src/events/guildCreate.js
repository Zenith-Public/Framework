const { Event } = require('../index');

module.exports = class guildCreate extends Event {
    constructor(client) {
        super(client, { name: 'guildCreate' });
    }

    async run(msg, guild) {
        await this.client.logger.info(`guildCreate fired- obj: ${guild} msg: ${msg} ID: ${guild.id} or ${msg.guild.id}`);
    }
};
