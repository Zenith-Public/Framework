const { Event } = require('../index');

module.exports = class Ready extends Event {
    constructor(client) {
        super(client, { name: 'ready' });
    }

    async run(msg) { //eslint-disable-line
        await this.client.logger.info('Bot is ready!');
    }
};
