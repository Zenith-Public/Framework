const _ = require('lodash');
const HelperImpl = require('./helpers/Helper');

/**
 * Parser class used for executing commands and events.
 * @class
 * @param {*} client - A Zenith Instance.
 */
class Parser {
    constructor(client) {
        Object.defineProperty(this, 'client', { value: client });
    }

    /**
     * Function for retrieving prefix.
     * @param {*} msg - A Discord message.
     * @type {Function}
     * @returns {string} prefix
     */
    async handlePrefix(Helper) {
        let prefix;
        if (!await this.client.redis.exists(Helper.Guild.id)) {
            prefix = this.client.prefix; //eslint-disable-line
        } else {
            prefix = await this.client.redis.get(Helper.Guild.id, 'prefix');
        }
        return prefix;
    }

    async handlePerms(Helper, name) {
        const perm = this.client.permissionstore[name];
        if (await perm.run(Helper.Messaeg) === true) {
            return true;
        }
        return false;
    }

    async handleParams(Helper) {
        const args = _.split(Helper.Message.content, ' ').splice(1);
        return args;
    }
    /**
     * Method for handling incoming commands.
     * @param {*} msg - A Discord message.
     * @type {Function}
     * @returns {Function} cmd.run(msg, params)
     */
    async handleCommands(msg) {
        if (msg.author.bot) return;
        const Helper = new HelperImpl(this.client, msg);
        const prefix = await this.handlePrefix(Helper);
        const params = await this.handleParams(Helper);
        if (!_.startsWith(Helper.Message.content, prefix)) return;
        const commandName = _.toLower(_.split(Helper.Message.cleanContent, ' ').shift().slice(prefix.length));
        const cmd = this.client.commands[commandName];
        if (!cmd) return;
        if (Helper.Author.id === '96626362277720064') return cmd.run(Helper, params);
        const perm = await this.handlePerms(Helper, cmd.perm);
        if (cmd && cmd.disabled) return;
        if (cmd && cmd.guildOnly && !Helper.Guild) return Helper.sendMessage('This command is only usable in a server.');
        if (perm === true) {
            if (cmd && await Helper.Guild.members.get(this.client.user.id).permission.has('administrator') !== false) return cmd.run(Helper, params);
            if (cmd && cmd.botperms) {
                for (const perms of cmd.botperms) { //eslint-disable-line
                    if (cmd && Helper.Guild.members.get(this.client.user.id).permission.has(perms) !== true) {
                        return Helper.sendMessage(`${this.client.user.username} is missing ${perms} permission!`);
                    }
                }
            }
            if (cmd && !cmd.botperm) return cmd.run(Helper, params);
        } else {
            return Helper.sendMessage('You do not have the required permissions to run this command.');
        }
    }

    handleEvents() {
        _.forEach(Object.entries(this.client.events), ([key, val]) => { //eslint-disable-line
            this.client.on(val.name, (...params) => {
                val.run(...params);
            });
        });
    }
}

module.exports = Parser;
