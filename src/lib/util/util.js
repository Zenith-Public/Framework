class Util {
    constructor() {
        throw new Error('This class is not allowed to be used with new');
    }
}

module.exports = Util;
