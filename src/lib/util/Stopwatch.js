const { performance } = require('perf_hooks');

class Stopwatch {
    constructor(digits = 2) {
        this.digits = digits;

        this.start = performance.now();

        this.end = null;
    }

    get duration() {
        return this.end ? this.end - this.start : performance.now() - this.start;
    }

    get running() {
        return Boolean(!this.end);
    }

    restart() {
        this.start = performance.now();
        this.end = null;
        return this;
    }

    reset() {
        this.start = performance.now();
        this.end = this.start;
        return this;
    }

    start() {
        if (!this.running) {
            this.start = performance.now() - this.duration;
            this.end = null;
        }
        return this;
    }

    stop() {
        if (this.running) this.end = performance.now();
        return this;
    }

    toString() {
        const time = this.duration;
        if (time >= 1000) return `${(time / 1000).toFixed(this.digits)}s`;
        if (time >= 1) return `${time.toFixed(this.digits)}ms`;
        return `${(time * 1000).toFixed(this.digits)}μs`;
    }
}

module.exports = Stopwatch;
