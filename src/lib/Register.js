const path = require('path');
const fs = require('fs');
const _ = require('lodash');
/**
 * Register class used for registering events and commands.
 * @class
 * @param {Client} client - A Zenith instance.
 * @param {string} commandPath - Path where commands are placed.
 * @param {string} eventPath - Path where events are placed.
 */
class Register {
    constructor(client, commandPath, eventPath) {
        Object.defineProperty(this, 'client', { value: client });

        /**
         * The path where commands are placed.
         * @type {String}
         */
        this.commandPath = commandPath;

        /**
         * The path where events are placed.
         * @type {String}
         */
        this.eventPath = eventPath;
    }
    /**
     * Method for registering all built in commands.
     * @type {Function}
     * @returns {Object} this
     */
    registerCoreCommands() {
        const commandFolder = fs.readdirSync(path.join(__dirname, '../commands'));
        _.forEach(commandFolder, com => {
            const Command = require(`../commands/${com}`);
            const cmd = new Command(this.client);
            if (this.client.commands[cmd.name]) { return; }
            this.client.commands[cmd.name] = cmd;
            if (cmd.aliases === undefined) { return; }
            for (const alias of cmd.aliases) {
                this.client.logger.info(`Alias: ${alias}`);
                this.client.commands[alias] = cmd;
            }
        });
        return this;
    }
    /**
     * Method for registering all built in commands.
     * @type {Function}
     * @returns {Object} this
     */
    registerCoreEvents() {
        const eventFolder = fs.readdirSync(path.join(__dirname, '../events'));
        _.forEach(eventFolder, eve => {
            const Event = require(`../events/${eve}`);
            const eva = new Event(this.client);
            if (this.client.events[eva.name]) { return; }
            this.client.events[eva.name] = eva;
        });
        this.client.parser.handleEvents();
        return this;
    }
    /**
     * Method for registering all built in commands.
     * @type {Function}
     * @returns {Object} this
     */
    registerCommands() {
        const commandFolder = fs.readdirSync(this.client.commandDir);
        _.forEach(commandFolder, com => {
            const Command = require(path.join(this.client.commandDir, com));
            const cmd = new Command(this.client);
            this.client.commands[cmd.name] = cmd;
            if (cmd.aliases === undefined) { return; }
            for (const alias of cmd.aliases) {
                this.client.logger.info(`Alias: ${alias}`);
                this.client.commands[alias] = cmd;
            }
        });
        return this;
    }
    /**
     * Method for registering all built in commands.
     * @type {Function}
     * @returns {Object} this
     */
    registerEvents() {
        const eventFolder = fs.readdirSync(this.client.eventDir);
        _.forEach(eventFolder, eve => {
            const Event = require(path.join(this.client.eventDir, eve));
            const event = new Event(this.client);
            this.client.events[event.name] = event;
        });
        this.client.parser.handleEvents();
        return this;
    }

    registerCorePermissions() {
        const permissionFolder = fs.readdirSync(path.join(__dirname, '/Permissions/Ranks'));
        _.forEach(permissionFolder, per => {
            const Permission = require(path.join(__dirname, '/Permissions/Ranks/', per));
            const perm = new Permission(this.client);
            this.client.permissionstore[perm.name] = perm;
        });
        return this;
    }
    registerPermissions() {
        const permissionFolder = fs.readdirSync(this.client.permissionDir);
        _.forEach(permissionFolder, per => {
            const Permission = require(path.join(this.client.permissionDir, per));
            const perm = new Permission(this.client);
            this.client.permissionstore[perm.name] = perm;
        });
        return this;
    }
}

module.exports = Register;
