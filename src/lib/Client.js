const Eris = require('eris');
const Register = require('./Register');
const Parser = require('./Parser');
const Config = require('./helpers/Config');
const Logger = require('./helpers/Logger');
const path = require('path');
const Redis = require('./Redis');
const _ = require('lodash');
// const colors = require('colors');

/**
 * Client class used for creating a Zenith instance.
 * @class
 * @param {Object} options - Options passed to the Client.
 * @param {String} options.config - File where config is placed.
 * @param {String} options.botdir - Where all bot files are located.
 */

class ZenithClient extends Eris.Client {
    constructor(options) {
        super(options.options);

        /**
         * File where config is placed.
         * @type {String}
         */
        this.configfile = options.config;
        if (!options.config) throw new Error('No Config file was provided.');

        /**
         * Where all bot files are located.
         * @type {String}
         */
        this.botdir = options.botdir;
        if (!options.botdir) throw new Error('No botdir was provided.');

        /**
         * Module responsible for handing config.
         * @type {Config}
         */
        this.config = new Config(this);

        /**
         * Module responsible for handing Logging.
         * @type {Logger}
         */
        this.logger = new Logger(this);

        /**
            * Gives the client the URL to connect to redis.
            * @type {string}
            */
        this.redisurl = this.config.get('redisurl', 'redis://localhost:6379/0');
        if (!this.redisurl) throw new Error('No Redis URL was provided');

        /**
         * Path where commands are placed.
         * @type {String}
         */
        this.commandDir = path.join(this.botdir, this.config.get('commandDir'));
        if (!this.commandDir) throw new Error('No Command Path was provided.');

        /**
         * Path where events are placed.
         * @type {String}
         */
        this.eventDir = path.join(this.botdir, this.config.get('eventDir'));
        if (!this.eventDir) throw new Error('No Event Path was provided.');

        /**
         * Path where permissions are placed.
         * @type {String}
         */
        this.permissionDir = path.join(this.botdir, this.config.get('permissionDir'));
        if (!this.permissionDir) throw new Error('No Permission path was provided');

        /**
         * Default prefix for the bot.
         * @type {String}
         */
        this.prefix = this.config.get('prefix', '>>');
        if (!_.isString(this.prefix)) throw new Error('Prefix must be a string.');

        /**
        * Token for the bot.
        * @type {string}
        */
        this.token = this.config.get('token');
        if (!this.config.get('token')) throw new Error('No token was provided.');

        /**
         * User ID for the bot's owner.
         * @type {array}
         */
        this.ownerID = this.config.get('ownerID', '96626362277720064');
        if (!this.ownerID) throw new Error('No Owner ID was provided.');

        /**
         * Object for storing commands.
         * @type {Object}
         */
        this.commands = {};

        /**
         * Object for storing events.
         * @type {Object}
         */
        this.events = {};

        /**
         * Object for storing permissions.
         * @type {Object}
         */
        this.permissionstore = {};

        /**
         * Module responsible for registering events and commands.
         * @type {Register}
         */
        this.register = new Register(this, this.commandPath, this.eventPath);

        /**
         * Module responsible for creating and handling the redis instance.
         * @type {Redis}
         */
        this.redis = new Redis(this, this.redisurl);

        /**
         * Module responsible for running commands and events
         * @type {Parser}
         */
        this.parser = new Parser(this);

        this.on('messageCreate', msg => this.parser.handleCommands(msg));
    }

    /**
     * Function for starting a Zenith instance.
     * @type {Function}
     */
    async start() {
        this.logger.info('Registering Commands and events!');
        await await this.register
            .registerPermissions()
            .registerCorePermissions()
            .registerCommands()
            .registerEvents()
            .registerCoreCommands()
            .registerCoreEvents();

        await this.logger.info(`Loaded: ${_.keys(this.commands).length} commands!`);
        await this.logger.info(`Loaded: ${_.keys(this.events).length} events!`);
        await this.logger.info(`Loaded: ${_.keys(this.permissionstore).length} permissions!`);
        if (process.env.CI) {
            this.logger.info('CI | Test completed!');
            this.logger.info('CI | Exiting with 0');
            process.exit(0);
        }
        this.logger.info('Connecting to discord..');
        await this.connect().catch(async err => { this.logger.info(`Couldn't connect to gateway: ${err}`); });
    }
}

module.exports = ZenithClient;
