const Client = require('./Client'); //eslint-disable-line
const { Member, Message } = require('eris'); //eslint-disable-line

/**
 * Event class used for creating Zenith Eventd.
 * @class
 * @param {Client} client - A Zenith instance.
 * @param {Object} options - Event options for a Zenith event.
 * @param {string} options.name - Event name.
 */
class Event {
    constructor(client, options) {
        Object.defineProperty(this, 'client', { value: client });

        /**
         * The name of the Event.
         * @type {string}
         */
        this.name = options.name;
    }

    /**
     * Main method for executing an Event.
     * @type {Function}
     * @param {objet} msg - Object which contains the discord message.
     */
    async run(msg) { //eslint-disable-line
        throw new Error(`The event "${this.event}" does not override the run method.`);
    }
}

module.exports = Event;
