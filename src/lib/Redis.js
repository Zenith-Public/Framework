const Redite = require('redite');

class Redis {
    constructor(client) {
        Object.defineProperty(this, 'client', { value: client });

        this.redis = new Redite({ url: client.redisurl });
        this.client.logger.debug('Connected!', 'Redis', 'Debug');
    }

    async exists(aa) {
        if (await this.redis[aa].exists()) {
            return true;
        }
        return false;
    }

    async get(proxy, main) {
        await this.redis[proxy][main].get;
    }

    async change(proxy, main, key) {
        await this.redis[proxy][main].set(key);
    }

    async set(proxy, main) {
        await this.redis[proxy].set({ main });
    }
}
module.exports = Redis;
