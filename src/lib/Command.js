const Client = require('./Client'); //eslint-disable-line

/**
 * Command class used for creating Zenith commands.
 * @class
 * @param {Client} client - A Zenith instance.
 * @param {Object} options - Command options for a Zenith command.
 * @param {string} options.name - Command name.
 * @param {array} options.alias - Additional command names
 * @param {string} options.command=No usage. - Command usage.
 * @param {string} options.description=No description. - Command description.
 * @param {boolean} options.guildOnly=false - Whether the command is only usable in a guild.
 * @param {boolean} options.ownerOnly=false - Whether the command is only to be used by the bot's owner.
 * @param {boolean} options.disable=false - Whether the command is disabled.
 * @param {array} options.botperms - Permission the bot needs to run the command
 * @param {string} options.perm - Permission level needed by the user.
 */
class Command {
    constructor(client, options) {
        Object.defineProperty(this, 'client', { value: client });

        /**
         * The name of the command.
         * @type {string}
         */
        this.name = options.name;

        /**
         * Alias for the command.
         * @type {array}
         */
        this.aliases = options.aliases;

        /**
         * The usage for the command.
         * @type {string}
         */
        this.usage = options.usage || 'No usage';

        /**
         * Description for the command.
         * @type {string}
         */
        this.description = options.description || 'No description';

        /**
         * If the command is guildOnly.
         * @type {boolean}
         */
        this.guildOnly = options.guildOnly || false;

        /**
         * If the command is ownerOnly.
         * @type {boolean}
         */
        this.ownerOnly = options.ownerOnly || false;

        /**
         * If the command is disabled.
         * @type {boolean}
         */
        this.disabled = options.disabled || false;

        /**
         * What permissions the user needs..
         * @type {boolean}
         */
        this.perm = options.perm || 'everyone';

        this.params = options.params;

        /**
         * What permissions the bot needs to run the command.
         * @type {string}
         */
        this.botperms = options.botperms;
        if (!this.name) throw new Error(`Constructor ${this.constructor.name} is missing a command name.`);
    }

    /**
     * Main method for executing a command.
     * @type {Function}
     * @param {objet} Helper - Helper Object.
     */
    async run(Helper) { //eslint-disable-line
        throw new Error(`The command "${this.name}" does not override the run method.`);
    }
}

module.exports = Command;
