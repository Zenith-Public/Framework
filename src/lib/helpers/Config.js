'use strict';

const Fs = require('fs-extra');
const _ = require('lodash');
const extendify = require('extendify');
const Cache = require('memory-cache');

class Config {
    constructor(client) {
        Object.defineProperty(this, 'client', { value: client });
        if (_.isNull(Cache.get('config'))) {
            Cache.put('config', this.raw());
        }
    }

    raw() {
        try {
            return require(this.client.configfile); //eslint-disable-line
        } catch (err) {
            throw err;
        }
    }

    get(key, defaultResponse) {
        let getObject;
        try {
            getObject = _.reduce(_.split(key, '.'), (o, i) => o[i], Cache.get('config'));
        } catch (err2) { _.noop(); }

        if (!_.isUndefined(getObject)) {
            return getObject;
        }

        return (!_.isUndefined(defaultResponse)) ? defaultResponse : undefined;
    }

    save(json, next) {
        if (!json || !_.isObject(json) || _.isNull(json) || !_.keys(json).length) {
            throw new Error('Invalid JSON was passed to Builder.');
        }

        Fs.writeJson(this.client.configfile, json, { spaces: 2 }, err => {
            if (!err) Cache.put('config', json);
            return next(err);
        });
    }

    modify(object, next) {
        if (!_.isObject(object)) return next(new Error('Function expects an object to be passed.'));
        const deepExtend = extendify({
            inPlace: false,
            arrays: 'replace',
        });
        const modifiedJson = deepExtend(Cache.get('config'), object);

        Fs.writeJson(this.client.configfile, modifiedJson, { spaces: 2 }, err => {
            if (err) return next(err);

            Cache.put('config', modifiedJson);
            return next();
        });
    }
}

module.exports = Config;
