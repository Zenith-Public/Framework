const Bunyan = require('bunyan');
const Path = require('path');

class Logger {
    constructor(client) {
        Object.defineProperty(this, 'client', { value: client });

        const log = Bunyan.createLogger({
            name: 'zenith',
            src: false,
            serializers: Bunyan.stdSerializers,
            streams: [
                {
                    level: this.client.config.get('logger.lvl', 'info'),
                    stream: process.stdout,
                },
                {
                    path: Path.join(this.client.config.get('logger.path', 'logs/'), 'zenith.log'),
                    type: 'rotating-file',
                    level: this.client.config.get('logger.lvl', 'info'),
                    period: this.client.config.get('logger.period', '1d'),
                },
            ],
        });
        return log;
    }
}
module.exports = Logger;
