'use strict';

/**
 * Helper class.
 * @class
 * @param {Object} msg - msg object from eris.
 * @param {Object} client - client object.
 */
class Helper {
    constructor(client, msg) {
        /**
         * Modified Client Object
         * @type {Object}
         */
        this.Client = client;

        /**
         * Client Object
         * @type {Object}
         */
        this.Bot = client;

        /**
         * Client.config Object
         * @type {Object}
         */
        this.Config = client.config;

        /**
         * msg object
         * @type {Object}
         */
        this.Message = msg;

        /**
         * msg.channel.guild object
         * @type {Object}
         */
        this.Guild = msg.channel.guild;

        /**
         * msg.channel object
         * @type {Object}
         */
        this.Channel = msg.channel;

        /**
         * msg.author object
         * @type {Object}
         */
        this.Author = msg.author;

        /**
         * this.client.redis object
         * @type {Object}
         */
        this.Redis = client.redis;

        /**
         * this.client.logger object
         * @type {Object}
         */
        this.Logger = client.logger;
        /**
         * Stopwatch require
         * @type {Object}
         */
        this.Stopwatch = require('./../util/Stopwatch');
    }

    /**
     * Function for sending a message.
     * @type {Function}
     * @param {String} content - What gets sent to the channel.
     */
    async sendMessage(content) {
        await this.Message.channel.createMessage(content);
    }
}

module.exports = Helper;
