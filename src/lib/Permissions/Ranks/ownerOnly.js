const { Permission } = require('../../../index');
const _ = require('lodash');

module.exports = class ownerOnly extends Permission {
    constructor(client) {
        super(client, { name: 'ownerOnly' });
    }

    async run(msg) {
        if (_.includes(this.client.ownerID, msg.author.id)) {
            return true;
        }
    }
};

