const { Permission } = require('../../../index');

module.exports = class everyone extends Permission {
    constructor(client) {
        super(client, { name: 'everyone' });
    }

    async run(msg) {
        if (msg && msg.member) {
            return true;
        }
    }
};
