const Client = require('../Client'); //eslint-disable-line
const { Member, Message } = require('eris'); //eslint-disable-line

/**
 * Permission class used for creating Zenith Permissions.
 * @class
 * @param {Client} client - A Zenith instance.
 * @param {Object} options - Permission options for a Zenith Permission.
 * @param {string} options.name - Permision level.
 */
class Permission {
    constructor(client, options) {
        Object.defineProperty(this, 'client', { value: client });

        /**
         * The name of the Permission.
         * @type {string}
         */
        this.name = options.name;
    }

    /**
     * Main method for checking a permission.
     * @type {Function}
     * @param {object} msg - Object which contains the discord message.
     */
    async run(msg) { //eslint-disable-line
        throw new Error(`The Permission "${this.name}" does not override the run method.`);
    }
}

module.exports = Permission;
