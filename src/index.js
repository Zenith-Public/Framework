module.exports = {
    Client: require('./lib/Client'),
    ZenithClient: require('./lib/Client'),

    Command: require('./lib/Command'),
    Event: require('./lib/Event'),

    Register: require('./lib/Register'),
    Parser: require('./lib/Parser'),

    util: require('./lib/util/util'),
    Stopwatch: require('./lib/util/Stopwatch'),

    Permission: require('./lib/Permissions/Permission'),

    Redis: require('./lib/Redis'),

    // Helpers

    Config: require('./lib/helpers/Config'),
    Logger: require('./lib/helpers/Logger'),
    Helper: require('./lib/helpers/Helper'),
};
