const { Command } = require('../index');
const exec = require('child_process').exec; //eslint-disable-line
const _ = require('lodash');

module.exports = class Exec extends Command {
    constructor(client) {
        super(client, {
            name: 'exec',
            perm: 'ownerOnly',
        });
    }

    async run(Helper) {
        const args = _.split(Helper.Message.content, ' ').slice(1);
        const code = args.join(' ');
        const stopwatch = new Helper.Stopwatch();
        let syncTime;
        // for loop using new config when!
        if (_.includes(args, 'start') || _.includes(args, 'cmd') || _.includes(args, 'powershell') || _.includes(args, 'shutdown') || _.includes(args, 'rm') || _.includes(args, 'del') || _.includes(args, 'bash') || _.includes(args, 'zsh') || _.includes(args, 'sh')) {
            Helper.sendMessage('I\'ll pass. Bot AI Protection:tm: v1.');
            return;
        }
        exec(code, (error, stdout, stderr) => {
            if (!error) {
                syncTime = stopwatch.toString();
                if (stdout) {
                    if (stdout.length > 1300) {
                        stdout = stdout.substr(stdout.length - 1299, stdout.length); //eslint-disable-line
                    }
                }
            }
            const output = stdout ?
                `**\`OUTPUT\`**${'```prolog'}\n${stdout}\n${'```'}` :
                '';

            const outerr = stderr ?
                `**\`ERROR\`**${'```prolog'}\n${stderr}\n${'```'}` :
                '';
            stopwatch.stop();
            Helper.sendMessage([output, outerr].join('\n'));
            return Helper.sendMessage(this.formatTime(syncTime));
        });
    }
    formatTime(syncTime) {
        return `⏱ ${syncTime}`;
    }
};

