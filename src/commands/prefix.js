const { Command } = require('../index');
const _ = require('lodash');

module.exports = class Prefix extends Command {
    constructor(client) {
        super(client, { name: 'prefix' });
    }

    async run(Helper) {
        const args = _.split(Helper.Message.content.split, ' ').splice(1);
        if (!args[0]) {
            await Helper.sendMessage('**Command Usage:**');
            await Helper.sendMessage('>>prefix set ! - This will set the new prefix as ! [WIP]');
            await Helper.sendMessage('>>prefix reset - This will reset the prefix to >> [WIP]');
            return;
        }
        if (!await Helper.Client.redis.exists(Helper.Guild.id)) {
            await Helper.Client.logger.info('creating redis thing for ', Helper.Guild.name);
            await Helper.Client.redis.set(Helper.Guild.id, "settings: {}, prefix: '>>'");
        }
        if (args[0] === 'set') {
            let prefix = JSON.stringify(args[1]);
            prefix = _.replace(prefix.replace, '"', '');
            prefix = _.replace(prefix.replace, '"', '');
            Helper.sendMessage(`Changing prefix to ${prefix}`);
            await Helper.Client.redis.change(Helper.Guild.id, 'prefix', prefix);
        }
        if (args[0] === 'reset') {
            const prefix = Helper.Client.prefix; //eslint-disable-line
            await Helper.Client.redis.change(Helper.Guild.id, 'prefix', prefix);
            await Helper.sendMessage(`Reset prefix to ${prefix}`);
        }
    }
};
