const { Command } = require('../index');
const exec = require('child_process').exec; //eslint-disable-line
const code = 'git pull';

module.exports = class Update extends Command {
    constructor(client) {
        super(client, {
            name: 'update',
            perm: 'ownerOnly',
        });
    }

    async run(Helper) {
        const stopwatch = new Helper.Stopwatch();
        let syncTime;
        let asyncTime;
        await exec(code, (error, stdout, stderr) => {
            if (!error) {
                asyncTime = stopwatch.toString();
                syncTime = stopwatch.toString();
                if (stdout) {
                    if (stdout.length > 1300) {
                        stdout = stdout.substr(stdout.length - 1299, stdout.length); //eslint-disable-line
                    }
                }
            }
            const output = stdout ?
                `**\`OUTPUT\`**${'```prolog'}\n${stdout}\n${'```'}` :
                '';

            const outerr = stderr ?
                `**\`ERROR\`**${'```prolog'}\n${stderr}\n${'```'}` :
                '';
            stopwatch.stop();
            Helper.sendMessage([output, outerr].join('\n'));
            return Helper.sendMessage(this.formatTime(syncTime, asyncTime));
        });
    }
    formatTime(syncTime, asyncTime) {
        return asyncTime ? `⏱ ${asyncTime}<${syncTime}>` : `⏱ ${syncTime}`;
    }
};
