const { Command } = require('../index');

module.exports = class Restart extends Command {
    constructor(client) {
        super(client, {
            name: 'restart',
            aliases: ['reboot'],
            perm: 'ownerOnly',
        });
    }

    async run(Helper) {
        await Helper.sendMessage('Restarting Bot..');
        process.exit(0);
    }
};
