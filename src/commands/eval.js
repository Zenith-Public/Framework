const { Command } = require('../index');
const hastebin = require('hastebin-gen');
const _ = require('lodash');

module.exports = class Eval extends Command {
    constructor(client) {
        super(client, {
            name: 'eval',
            aliases: ['ev', 'sh'],
            perm: 'ownerOnly',
            params: ['code'],
        });
    }

    async run(Helper) {
        let depth = 0;
        const args = _.split(Helper.Message.content, ' ').splice(1);
        if (args[0] === '--async' || args[0] === 'a') {
            if (args[1] === '--depth') {
                depth = args[2];
                delete args[1];
                delete args[2];
            }
        } else if (args[0] === '--depth') {
            depth = args[1];
            delete args[1];
            delete args[0];
        }
        if (!args[2]) {
            if (!args[0]) {
                return Helper.sendMessage('Error 418? https://google.com/teapot');
            }
        }
        if (args[0] === '--async' || args[0] === '-a') {
            delete args[0];
            const stopwatch = new Helper.Stopwatch();
            let syncTime;
            try {
                const code = args.join(' ');
                const Result = require('util').inspect(await eval(code), null, depth); //eslint-disable-line
                syncTime = stopwatch.toString();
                await stopwatch.stop();
                if (Result.length > 2000) {
                    return await hastebin(Result, 'js').then(async req => {
                        await Helper.sendMessage(`Output too long, it has been posted to ${req}`);
                    }).catch(async err => {
                        await Helper.sendMessage(`I appear to have run into some sort of error.\n\n${err}`);
                    });
                }
                await Helper.sendMessage({
                    embed: {
                        title: 'Async Eval:',
                        author: {
                            name: Helper.Author.username,
                            icon_url: Helper.Author.avatarURL //eslint-disable-line
                        },
                        color: 0x008000,
                        fields: [
                            {
                                name: 'Code:',
                                value: `\`\`\`js\n${code}\`\`\``,
                                inline: false,
                            },
                            {
                                name: 'Result:',
                                value: `\`\`\`js\n${Result}\`\`\``,
                                inline: false,
                            },
                        ],
                    },
                });
                return await Helper.sendMessage(this.formatTime(syncTime));
            } catch (err) {
                if (!syncTime) syncTime = stopwatch.toString();
                return Helper.sendMessage(`\`\`\`js\n${err}\`\`\``);
            }
        } else {
            const stopwatch = new Helper.Stopwatch();
            let syncTime;
            try {
                const code = args.join(' ');
                let Result = require('util').inspect(eval(code), null, depth); //eslint-disable-line
                syncTime = stopwatch.toString();
                const tokena = _.replace(Helper.Config.get('token'), /\./g, '.');
                const rea = new RegExp(tokena, 'g');
                const token = _.replace(Helper.Client.token, /\./g, '.');
                const re = new RegExp(token, 'g');
                Result = _.replace(Result, re, '「ｒｅｄａｃｔｅｄ」');
                Result = _.replace(Result, rea, '「ｒｅｄａｃｔｅｄ」');
                await stopwatch.stop();
                if (Result.length > 1024) {
                    return await hastebin(Result, 'js').then(async req => {
                        await Helper.sendMessage(`Output too long, it has been posted to ${req}`);
                    }).catch(async err => {
                        await Helper.sendMessage(`I appear to have run into some sort of error.\n\n${err}`);
                    });
                }
                await Helper.sendMessage({
                    embed: {
                        title: 'Eval:',
                        author: {
                            name: Helper.Author.username,
                                icon_url: Helper.Author.avatarURL //eslint-disable-line
                        },
                        color: 0x008000,
                        fields: [
                            {
                                name: 'Code:',
                                value: `\`\`\`js\n${code}\`\`\``,
                                inline: false,
                            },
                            {
                                name: 'Result:',
                                value: `\`\`\`js\n${Result}\`\`\``,
                                inline: false,
                            },
                        ],
                    },
                });

                return Helper.sendMessage(this.formatTime(syncTime));
            } catch (err) {
                if (!syncTime) syncTime = stopwatch.toString();
                return Helper.sendMessage(`\`\`\`js\n${err}\`\`\``);
            }
        }
    }
    formatTime(syncTime) {
        return `⏱ ${syncTime}`;
    }
};
