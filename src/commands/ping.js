const { Command } = require('../index');

module.exports = class Ping extends Command {
    constructor(client) {
        super(client, { name: 'ping' });
    }

    async run(Helper) {
        const message = await Helper.sendMessage('Ping...');
        await message.edit(`Pong! WS: \`${Helper.Client.shards.get(Helper.Client.guildShardMap[Helper.Client.channelGuildMap[message.channel.id]] || 0).latency}\`ms.`);
    }
};
