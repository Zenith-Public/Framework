const { Command } = require('../index');
const fs = require('fs');
const _ = require('lodash');

module.exports = class Reload extends Command {
    constructor(client) {
        super(client, {
            name: 'reload',
            perm: 'ownerOnly',
        });
    }

    async run(Helper) {
        const args = _.split(Helper.Message.content, ' ').splice(1);
        if (!args[0]) {
            await Helper.sendMessage('Reloading all commands!');
            const old = this.client.commands;
            this.client.commands = [];
            _.forEach(_.keys(old), async com => {
                if (fs.existsSync(`${Helper.Client.commandDir}/${com}.js`)) {
                    try {
                        await delete require.cache[require.resolve(`${Helper.Client.commandDir}/${com}.js`)];
                    } catch (err) {
                        Helper.Logger.error(err);
                    }
                } else {
                    try {
                        await delete require.cache[require.resolve(`./${com}.js`)];
                    } catch (err) {
                        Helper.Logger.error(err);
                    }
                }
            });

            await this.client.register
                .registerCommands()
            // .registerEvents();
                .registerCoreCommands()
                .registerCoreEvents();
            return Helper.sendMessage('Reloaded all commands!');
        }
        await Helper.sendMessage(`Reloading ${args[0]} command!`);
        this.client.commands = [];
        if (fs.existsSync(`${this.client.commandDir}/${args[0]}.js`)) {
            await delete require.cache[require.resolve(`${this.client.commandDir}/${args[0]}.js`)];
        } else {
            await delete require.cache[require.resolve(`./${args[0]}.js`)];
        }
        await this.client.register
            .registerCommands()
        // .registerEvents();
            .registerCoreCommands()
            .registerCoreEvents();
        return Helper.sendMessage('Reloaded!');
    }
};
