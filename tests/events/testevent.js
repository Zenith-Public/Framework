const { Event } = require('../../src/index');

module.exports = class TestEvent extends Event {
    constructor(client) {
        super(client, { name: 'test' });
    }

    async run(msg) { //eslint-disable-line
        this.client.logger.debug('testevent fired!');
    }
};
