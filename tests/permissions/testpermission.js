const { Permission } = require('../../src/index');

module.exports = class TestPermission extends Permission {
    constructor(client) {
        super(client, { name: 'test' });
    }

    async run(msg) { //eslint-disable-line
        this.client.logger.debug('testptermission fired! fired!');
    }
};
