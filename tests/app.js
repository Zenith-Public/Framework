const { Client } = require('../src/index');
const path = require('path');

const config = path.join(__dirname, './config.json');

class Zenith extends Client {
    constructor() {
        super({
            config,
        });
    }
}


new Zenith().start();
