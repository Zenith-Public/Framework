const { Command } = require('../../src/index');

module.exports = class TestCmd extends Command {
    constructor(client) {
        super(client, { name: 'test' });
    }

    async run(msg) { //eslint-disable-line
        this.client.logger.debug('testcmd fired!');
    }
};
