# Zenith
![CC BY-NC-SA 3.0](https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png)
 This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-nc-sa/3.0/).
## What is this
Zenith is a framework made to be as simple as possible with no core modification needed for your bot.
DOCS are WIP.

### What does it use?
Originally it started off by using discord.js but I was convinced that eris was better so here we are.
- Redis
- Eris
- NodeJS
- Performance-now (Used for stopwatch)
- Hastebin-gen (Used for overflowing msgs).
- Redite (Redis client for nodejs)

The doc link will be here eventually but you can just use ./tests/app.js and remake it.
If you want to edit any core commands just copy it from /src/commands and add it to your command folder and it will load your custom one rather than the core one.
